
-- A. find all artists that has letter D in its name.
SELECT * FROM artists WHERE name LIKE "%d%";

-- B. Find all songs that has a length of less than 230.
SELECT * FROM songs WHERE length < 230;

-- C.Join the albums and songs tables. (Only show the album name, song name, and song length)
SELECT album_title, song_name, length FROM albums
    JOIN songs ON albums.id = songs.album_id;

-- D.Join the artists and albums(Find all albums that has letter A in its name)
SELECT * FROM artists
    JOIN albums ON artists.id = albums.artist_id WHERE album_title LIKE "%a%";

-- E. Sort the albums in Z-A order. (Show only the first 4 secs)
SELECT * FROM albums ORDER BY album_title DESC LIMIT 4;

-- F. Join the albums and songs table. (Sort albums from Z-A and sort songs from A-Z)
SELECT * FROM albums
    JOIN songs ON albums.id = songs.album_id ORDER BY album_title DESC, song_name ASC;
